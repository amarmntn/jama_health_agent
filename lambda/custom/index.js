/* eslint-disable  func-names */
/* eslint quote-props: ["error", "consistent"]*/
/**
 * This sample demonstrates a simple skill built with the Amazon Alexa Skills
 * nodejs skill development kit.
 * This sample supports multiple lauguages. (en-US, en-GB, de-DE).
 * The Intent Schema, Custom Slots and Sample Utterances for this skill, as well
 * as testing instructions are located at https://github.com/alexa/skill-sample-nodejs-fact
 **/

'use strict';
const Alexa = require('alexa-sdk');

//=========================================================================================================================================
//TODO: The items below this comment need your attention.
//=========================================================================================================================================

//Replace with your app ID (OPTIONAL).  You can find this value at the top of your skill's page on http://developer.amazon.com.
//Make sure to enclose your value in quotes, like this: const APP_ID = 'amzn1.ask.skill.bb4045e6-b3e8-4133-b650-72923c5980f1';
const APP_ID = "amzn1.ask.skill.bb2336a3-0044-456d-b8d2-beb4512d4139";

const SKILL_NAME = 'Jama Health Agent';
const HELP_MESSAGE = 'What can I help you with?';
const HELP_REPROMPT = 'What can I help you with?';
const STOP_MESSAGE = 'Goodbye!';

//=========================================================================================================================================
//TODO: Replace this data with your own.  You can find translations of this data at http://github.com/alexa/skill-sample-node-js-fact/data
//=========================================================================================================================================

const GREETING = [
    'Hello',
    'Welcome to Health care companion agent',
];

const GET_AGE = "How old you are?";
const GET_NAME = "What is your name?";
const SAY_HI_1 = "Hello ";
const SAY_HI_2 = " ,Nice to meet you";
const GET_SYMPTOM = "Please tell more about your symptoms?";

//=========================================================================================================================================
//Custom javascript functions
//=========================================================================================================================================

function getIntialGreeting(obj,userName){
    
    //Intial Greetings , If name is known
    const INITIALGREETINGS = [
        'Hello '+ userName +', I am Jama. I have not spoken to you for some time. How are you?',
        'Hi '+ userName +', I am Jama. Hope you are well!'
    ];
    
    const intitialGreetingArr = INITIALGREETINGS;
    const intitialGreetingIndex = Math.floor(Math.random() * intitialGreetingArr.length);
    
    //generating card for alexa app
    const speech_output = intitialGreetingArr[intitialGreetingIndex];
    const card_title = "Greetings from Jama";
    const card_content = intitialGreetingArr[intitialGreetingIndex];
    const image_objects ={
        "smallImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/12_health_1.png",
        "largeImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/12_health_1.png"
    };
    obj.emit(':tellWithCard',speech_output,card_title,card_content,image_objects);
    
}

function captureMoodOfUser(obj,userName,mood){
    
    //Get more symptoms through these random dialogs
    const addSymptoms = [
        'Ok noted .Sorry to hear that '+ userName +', How long you been suffering?'
    ];
    
    const addSymptomsArr = addSymptoms;
    const addSymptomsIndex = Math.floor(Math.random() * addSymptomsArr.length);

    //generating card for alexa app
    const speech_output = addSymptomsArr[addSymptomsIndex];
    const card_title = "Your have "+mood;
    const card_content = addSymptomsArr[addSymptomsIndex];
    const image_objects ={
        "smallImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/12_health_1.png",
        "largeImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/12_health_1.png"
    };
    obj.emit(':tellWithCard',speech_output,card_title,card_content,image_objects);
 
}

function captureMoodDurationOfUser(obj,userName,moodDuration){
    
    //Get more symptoms through these random dialogs
   
    var getAllSymptomsArr = obj.attributes['mood'];
    var getmoodDuration = obj.attributes['moodduration'];
    var getAllSymptomsStr = getAllSymptomsArr.toString();
    var strFormatted = getAllSymptomsStr.replace(/,([^,]*)$/,' and '+'$1');
    
    var outText = 'You seem to have '+strFormatted+ ' from '+ getmoodDuration+' days Please take treatment from doctor';
  
    //generating card for alexa app
    const speech_output = outText;
    const card_title = 'Symptoms Summary';
    const card_content = outText;
    const image_objects ={
        "smallImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/12_health_1.png",
        "largeImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/12_health_1.png"
    };
    obj.emit(':tellWithCard',speech_output,card_title,card_content,image_objects);
 
}

function captureSetAppointmentOfUser(obj,userName){
    //Get more symptoms through these random dialogs
    const addAppointment = [
        'Ok '+ userName +', I will create an appointment with doctor So when do you want to go?',
        'Ok '+ userName +', Let me schedule an appointment with doctor When do you want me to schedule?',
       
        
    ];
    
    const addAppointmentArr = addAppointment;
    const addAppointmentIndex = Math.floor(Math.random() * addAppointmentArr.length);

    //generating card for alexa app
    const speech_output = addAppointmentArr[addAppointmentIndex];
    const card_title = "Appointment with doctor";
    const card_content =  addAppointmentArr[addAppointmentIndex];
    const image_objects ={
        "smallImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/appointment.png",
        "largeImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/appointment.png"
    };
    obj.emit(':tellWithCard',speech_output,card_title,card_content,image_objects);
 
}

function SummaryOfAppointment(obj,userName,appDate,appTime){
    
    var outText = 'Ok, Noted. I have booked an appointment with Doctor '+ userName+' for '+appDate+' at '+appTime;
  
    //generating card for alexa app
    const speech_output = outText;
    const card_title = 'Appointment Created';
    const card_content = outText;
    const image_objects ={
        "smallImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/appointment.png",
        "largeImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/appointment.png"
    };
    obj.emit(':tellWithCard',speech_output,card_title,card_content,image_objects);
 
}

function getMyPrescription(obj,userName){
    
    //Get more symptoms through these random dialogs
   
    var getAllPrescriptionsArr = obj.attributes['prescriptions'];
    var getAllPrescriptionsStr = getAllPrescriptionsArr.toString();
    var strFormatted = getAllPrescriptionsStr.replace(/,([^,]*)$/,' and '+'$1');
    
    var outText ='Hello '+ userName+',your prescriptions are '+strFormatted;
  
    //generating card for alexa app
    const speech_output = outText;
    const card_title = 'Prescriptions Summary';
    const card_content = outText;
    const image_objects ={
        "smallImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/12_health_1.png",
        "largeImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/12_health_1.png"
    };
    obj.emit(':tellWithCard',speech_output,card_title,card_content,image_objects);
 
 
}

function captureAppointmentOfUser(obj,userName){
    
    if( obj.attributes['appointmentList']){
        var outText = 'You have an appointment with Doctor '+ userName +' on '+obj.attributes['appointmentList'];
      
        //generating card for alexa app
        const speech_output = outText;
        const card_title = 'Appointment Summary';
        const card_content = outText;
        const image_objects ={
            "smallImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/appointment.png",
            "largeImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/appointment.png"
        };
        obj.emit(':tellWithCard',speech_output,card_title,card_content,image_objects);
        
    }else{
        
        var outTextf = 'You dont have any appointments with doctors';
      
        //generating card for alexa app
        const speech_output = outTextf;
        const card_title = 'No Appointments';
        const card_content = outTextf;
        const image_objects ={
            "smallImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/appointment.png",
            "largeImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/appointment.png"
        };
        obj.emit(':tellWithCard',speech_output,card_title,card_content,image_objects);
    }
 
}

function captureAppointmentOfDoctor(obj,userName){
    
    if( obj.attributes['appointmentList']){
        var outText = 'Hi Doctor, You have an appointment with Patient '+ userName +' on '+obj.attributes['appointmentList'];
      
        //generating card for alexa app
        const speech_output = outText;
        const card_title = 'Appointment Summary';
        const card_content = outText;
        const image_objects ={
            "smallImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/appointment.png",
            "largeImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/appointment.png"
        };
        obj.emit(':tellWithCard',speech_output,card_title,card_content,image_objects);
        
    }else{
        
        var outTextf = 'Hi Doctor, You dont have any appointments with patients';
      
        //generating card for alexa app
        const speech_output = outTextf;
        const card_title = 'No Appointments';
        const card_content = outTextf;
        const image_objects ={
            "smallImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/appointment.png",
            "largeImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/appointment.png"
        };
        obj.emit(':tellWithCard',speech_output,card_title,card_content,image_objects);
    }
 
}

function confirmPhone(obj,userName,phone){
    var outText = 'Ok '+ userName+' your contact number is saved';
  
    //generating card for alexa app
    const speech_output = outText;
    const card_title = 'Contact Number:'+phone;
    const card_content = outText;
    const image_objects ={
        "smallImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/appointment.png",
        "largeImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/appointment.png"
    };
    obj.emit(':tellWithCard',speech_output,card_title,card_content,image_objects);
 
}

function doctorAddPrescription(obj,userName,prescriptionItem){
    var outText = 'Ok noted.';
  
    //generating card for alexa app
    const speech_output = outText;
    const card_title = 'Prescription Added';
    const card_content = prescriptionItem;
    const image_objects ={
        "smallImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/12_health_1.png",
        "largeImageUrl":"https://s3-eu-west-1.amazonaws.com/jamaskill/12_health_1.png"
    };
    obj.emit(':tellWithCard',speech_output,card_title,card_content,image_objects);
 
}


//=========================================================================================================================================
//Editing anything below this line might break your skill.
//=========================================================================================================================================

const handlers = {
    
    //Intent    : Default launch request
    'LaunchRequest': function () {
            const greetingArr = GREETING;
            const greetingIndex = Math.floor(Math.random() * greetingArr.length);
            this.emit(':tell',greetingArr[greetingIndex]);
            
            //Checking for userName in DynamoDB
            if(this.attributes['userName']){
                getIntialGreeting(this,this.attributes['userName']);
            } else {
                this.emit(':ask',GET_NAME);
            }
        },
        
        //Intent    : MyNameIs
        //Slot      : name      
        'MyNameIs': function () { 
            const userName = this.event.request.intent.slots.name.value;
            this.attributes['userName'] = userName;
            getIntialGreeting(this,this.attributes['userName']);
        },
        
        //Intent    : getfeeling
        //Slot      : geltData      
        'getfeeling': function () { 
            const mood = this.event.request.intent.slots.feltData.value;
            this.attributes['mood'].push(mood);
            this.attributes['moodUpdate'] = new Date().toLocaleString();
            captureMoodOfUser(this,this.attributes['userName'],mood);    
        },

        //Intent    : getfeelingDate
        //Slot      : feltDate      
        'getfeelingDate': function () { 
            const moodDuration = this.event.request.intent.slots.feltDate.value;
            this.attributes['moodduration']= moodDuration;
            captureMoodDurationOfUser(this,this.attributes['userName'],moodDuration);    
        },
        
        //Intent    : bookAppointment
        //Slot      : null      
        'bookAppointment': function () { 
            captureSetAppointmentOfUser(this,this.attributes['userName']);
                
        },
        
        'getPhone': function (){
            const phone = this.event.request.intent.slots.phone.value;
            this.attributes['phone']= phone;
            confirmPhone(this,this.attributes['userName'],phone);
        },
        
        'getAppointment': function (){
            captureAppointmentOfUser(this,this.attributes['userName']);  
        },
        
        'getAppointmentDoctor': function (){
            captureAppointmentOfDoctor(this,this.attributes['userName']);  
        },
        
        //Intent    : getAppointmentDate
        //Slot      : {appointmentDate},{appointmentTime}      
        'getAppointmentDate': function () { 
            const appointmentDate = this.event.request.intent.slots.appointmentDate.value;
            const appointmentTime = this.event.request.intent.slots.appointmentTime.value;
            var appStr = appointmentDate +' '+ appointmentTime;
            
            this.attributes['appointmentList'] = appStr;
            this.attributes['appointmentStatus'] = "pending";
            SummaryOfAppointment(this,this.attributes['userName'],appointmentDate,appointmentTime);    
        },
        
        //Intent    : doctorPrescription
        //Slot      : {null}      
        'doctorPrescription': function () { 
            this.emit(':ask','Doctor '+this.attributes['userName']+', tell me what to add? I will be listening');   
        },
        
        //Intent    : getPrescription
        //Slot      : {null}      
        'getPrescription': function () { 
            const prescriptionItem = this.event.request.intent.slots.prescription.value;
            this.attributes['prescriptions'].push(prescriptionItem);
            //this.emit(':tell',prescriptionItem);
            doctorAddPrescription(this,this.attributes['userName'],prescriptionItem);    
        },
        
        //Intent    : getPrescription
        //Slot      : {null}      
        'getUserPrescription': function () { 
            getMyPrescription(this,this.attributes['userName']);    
        },

        
    'AMAZON.HelpIntent': function () {
        const speechOutput = HELP_MESSAGE;
        const reprompt = HELP_REPROMPT;

        this.response.speak(speechOutput).listen(reprompt);
        this.emit(':responseReady');
    },
    'AMAZON.CancelIntent': function () {
        this.response.speak(STOP_MESSAGE);
        this.emit(':responseReady');
    },
    'AMAZON.StopIntent': function () {
        this.response.speak(STOP_MESSAGE);
        this.emit(':responseReady');
    },
};

exports.handler = function (event, context, callback) {
    const alexa = Alexa.handler(event, context, callback);
    alexa.APP_ID = APP_ID;
    alexa.dynamoDBTableName = 'userData';
    alexa.registerHandlers(handlers);
    alexa.execute();
};
